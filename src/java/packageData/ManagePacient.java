
package packageData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ManagePacient {
    
    public void update(Connection con,  Pacient pacient) throws Exception{
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement("update pacient set nss =?, name=?, surname=? where id=?");
            
            st.setString(1, pacient.getNss());
            st.setString(2, pacient.getName());
            st.setString(3, pacient.getSurname());
            st.setInt(4, pacient.getId());
            st.executeUpdate();
        }catch (SQLException e){
            
            throw new Exception("There was a problem to update the pacient " + e.getMessage());
            
        }finally {
            if(st != null){
                st.close();
            }
        }
    }
    
    public void delete (Connection con, Pacient pacient) throws Exception{
        
        PreparedStatement st = null;
        
        try{
            
            st = con.prepareStatement("delete form pacient where nss=?");
            st.setString (1, pacient.getNss());
            
            st.executeUpdate();
            
        }catch(SQLException e){
            
            throw new Exception ("There was a problem to delete the pacient " + e.getMessage());
                        
        }finally{
            if(st != null){
                
                st.close();
            }
            
        }
    }
    
    public void insert(Connection con, Pacient pacient) throws Exception{
        
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement("insert into pacient(nss, name, surname) values(?,?,?)");
           
            st.setString(1, pacient.getNss());
            st.setString(2, pacient.getName());
            st.setString(3, pacient.getSurname());
            
            st.executeUpdate();
            
        }catch(SQLException e){
            
            throw new Exception("There was a problem to insert the pacient " + e.getMessage());
            
        }finally{
            
            if(st != null){
                st.close();
            }
        }
    }
    public Pacient GetByNss (Connection con, String nss) throws Exception{
        
        Pacient pacient = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            st = con.prepareStatement("select * from pacient where nss=" + nss);
            
            rs = st.executeQuery();
            
            while(rs.next()){
                pacient = new Pacient();
                pacient.setId(rs.getInt("id"));
                pacient.setNss(rs.getString("nss"));
                pacient.setName(rs.getString("name"));
                pacient.setSurname(rs.getString("surname"));
            }
            
        }catch(SQLException e){
            
            throw new Exception ("There was a problem to get the pacient " + e.getMessage());
            
        }finally{
            
            if(rs != null){
                rs.close();
            }
            if(st != null){
                st.close();
            }
        }
        return pacient;
    }
    
        public Pacient GetById (Connection con, int id) throws Exception{
        
        Pacient pacient = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            st = con.prepareStatement("select * from pacient where id=" + id);
            
            rs = st.executeQuery();
            
            while(rs.next()){
                pacient = new Pacient();
                pacient.setId(rs.getInt("id"));
                pacient.setNss(rs.getString("nss"));
                pacient.setName(rs.getString("name"));
                pacient.setSurname(rs.getString("surname"));
            }
            
        }catch(SQLException e){
            
            throw new Exception ("There was a problem to get the pacient " + e.getMessage());
            
        }finally{
            
            if(rs != null){
                rs.close();
            }
            if(st != null){
                st.close();
            }
        }
        
        return pacient;
    }

}
