
package packageData;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionDB {
    public Connection OpenConnection() throws Exception{
        Connection con;
        
        try{
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/pacient");
            con = ds.getConnection();
            
            return con;
        }catch (SQLException | NamingException e){
            throw new Exception("There was impossible to connect: " + e.getMessage());
        }
    }
    
    public void CloseConnection(Connection con) throws Exception{
        try{
            if(con != null){
                con.close();
            }
            
        }catch (SQLException e){
            throw new Exception ("There was impossible to close the connection: " + e.getMessage());

        }
    }
}
